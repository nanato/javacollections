public class LastDigit {
    public static void main(String[] args) {
        boolean s  = lastDigit(7, 17);
        System.out.println(s);
    }
    private static boolean lastDigit(int a, int b) {
        return (a % 10 == b % 10);
    }
}
